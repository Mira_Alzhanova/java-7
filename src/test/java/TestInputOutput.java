import house.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class TestInputOutput {

    @Test
    public void testReadWriteByte(){
        int[] array = new int[]{257, 81, 12, 13};
        Assert.assertArrayEquals(array, InputOutput.readWriteByte(array));
    }

    @Test
    public void testReadWrite(){
        int[] array = new int[]{257, 81, 12, 13};
        Assert.assertArrayEquals(array, InputOutput.readWrite(array));
    }

    @Test
    public void testReadArrayFromFileFromPos(){
        int[] array = new int[]{1, 2, 3, 4, 5, 6};
        InputOutput.writeArrayToFile(array);

        int[] expected = new int[] {4, 5, 6};
        Assert.assertArrayEquals(array, InputOutput.readArrayFromFileFromPos(0));
    }

    @Test
    public void testGetFilesWithResolution() throws LogicException {
        File[] expected = new File[]{
                new File("/Users/mira/Desktop/Block7/Test/test4.txt"),
                new File("/Users/mira/Desktop/Block7/Test/test1.txt"),
                new File("/Users/mira/Desktop/Block7/Test/test2.txt"),
                new File("/Users/mira/Desktop/Block7/Test/test3.txt")
        };
        Assert.assertArrayEquals(expected,
                InputOutput.getFilesWithResolution("/Users/mira/Desktop/Block7/Test", ".txt"));
    }

    @Test
    public void testSerializeDeserializeHouse() throws DateException {
        ArrayList<Person> persons1 = new ArrayList<>();
        ArrayList<Person> persons2 = new ArrayList<>();

        persons1.add(new Person("Barny", "Stinson", "",
                new Date(1987, 4, 23)));
        persons1.add(new Person("Mark", "Manson", "",
                new Date(1992, 7, 15)));
        persons2.add(new Person("Cris", "Martin", "",
                new Date(1972, 1, 28)));

        ArrayList<Flat> flats = new ArrayList<>();

        flats.add(new Flat(77, 60, persons1));
        flats.add(new Flat(28, 73, persons2));

        Person houseSenior = new Person("Marcus", "Hietala", "",
                new Date(1980, 6, 8));

        House house = new House("cddf3z26", "Vains 3", houseSenior, flats);

        try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            InputOutput.serializeHouse(house, baos);
            try(ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray())){
                Assert.assertEquals(house, InputOutput.deserializeHouse(bais));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSerializeHouseToJacksonStringAndDeserializeHouseFromJacksonString() throws DateException, IOException {
        ArrayList<Person> persons1 = new ArrayList<>();
        ArrayList<Person> persons2 = new ArrayList<>();

        persons1.add(new Person("Barny", "Stinson", "",
                new Date(1987, 4, 23)));
        persons1.add(new Person("Mark", "Manson", "",
                new Date(1992, 7, 15)));
        persons2.add(new Person("Cris", "Martin", "",
                new Date(1972, 1, 28)));

        ArrayList<Flat> flats = new ArrayList<>();

        flats.add(new Flat(77, 60, persons1));
        flats.add(new Flat(28, 73, persons2));

        Person houseSenior = new Person("Marcus", "Hietala", "",
                new Date(1980, 6, 8));

        House house = new House("cddf3z26", "Vains 3", houseSenior, flats);

        String jsonResult = InputOutput.serializeHouseToJacksonString(house);

        House deserializedHouse = InputOutput.deserializeHouseFromJacksonString(jsonResult);

        Assert.assertEquals(house, InputOutput.deserializeHouseFromJacksonString(jsonResult));
    }
}
