import com.google.gson.Gson;
import house.House;

import java.io.*;
import java.util.ArrayList;

public class InputOutput {
    public static int[] readWriteByte(int[] array){
        int[] res = new int[array.length];

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             DataOutputStream dos = new DataOutputStream(baos)){
            for(int i: array){
                dos.writeInt(i);
            }

            try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()))){
                for(int i = 0; i < res.length; i++){
                    res[i] = dis.readInt();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static int[] readWrite(int[] array){
        int[] res = new int[array.length];
        try (StringWriter sw = new StringWriter()){
            for (int i: array){
                sw.write(i);
            }

            try (StringReader sr = new StringReader(sw.toString())){
                for (int i = 0; i < res.length; i++){
                    res[i] = sr.read();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static void writeArrayToFile(int[] array){
        try (RandomAccessFile raf = new RandomAccessFile("/Users/mira/Desktop/Block7/Test/test4.txt", "rw")){
            for (int i: array){
                raf.writeInt(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int[] readArrayFromFileFromPos(int pos){
        int[] res = new int[0];
        try (RandomAccessFile raf = new RandomAccessFile("/Users/mira/Desktop/Block7/Test/test2.txt", "r")){
            res = new int[(int)raf.length()/4 - pos];
            raf.seek(4*pos);

            for (int i = 0; i < res.length; i++){
                res[i] = raf.readInt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static File[] getFilesWithResolution(String path, String resolution) throws LogicException {
        File file = new File(path);
        if(!file.isDirectory()){
            throw new LogicException("Not a directory");
        }

        ArrayList<File> res = new ArrayList<>();
        File[] temp = file.listFiles();
        for (File f: temp){
            if(f.toString().endsWith(resolution) && !f.isDirectory()){
                res.add(f);
            }
        }

        return res.toArray(new File[0]);
    }

    public static void serializeHouse(House house, OutputStream os){
        try(ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeObject(house);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static House deserializeHouse(InputStream is){
        House house = null;
        try(ObjectInputStream ois = new ObjectInputStream(is)) {
            house = (House) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return house;
    }

    public static String serializeHouseToJacksonString(House house)  {
        return new Gson().toJson(house);
    }

    public static House deserializeHouseFromJacksonString(String jsonString) {
        return new Gson().fromJson(jsonString, House.class);
    }
}

