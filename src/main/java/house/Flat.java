package house;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Flat implements Serializable {
    private int flatNumber;
    private int flatArea;
    private List<Person> owners;

    public Flat(int flatNumber, int flatArea, List<Person> owners) {
        this.flatNumber = flatNumber;
        this.flatArea = flatArea;
        this.owners = owners;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public int getFlatArea() {
        return flatArea;
    }

    public List<Person> getOwners() {
        return owners;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return flatNumber == flat.flatNumber &&
                flatArea == flat.flatArea &&
                Objects.equals(owners, flat.owners);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flatNumber, flatArea, owners);
    }
}
