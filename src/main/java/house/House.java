package house;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class House implements Serializable {
    private String uniqueIdentifier;
    private String address;
    private Person houseSenior;
    private List<Flat> flats;

    public House(String uniqueIdentifier, String address, Person houseSenior, List<Flat> flats) {
        this.uniqueIdentifier = uniqueIdentifier;
        this.address = address;
        this.houseSenior = houseSenior;
        this.flats = flats;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    public String getAddress() {
        return address;
    }

    public Person getHouseSenior() {
        return houseSenior;
    }

    public List<Flat> getFlats() {
        return flats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return Objects.equals(uniqueIdentifier, house.uniqueIdentifier) &&
                Objects.equals(address, house.address) &&
                Objects.equals(houseSenior, house.houseSenior) &&
                Objects.equals(flats, house.flats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uniqueIdentifier, address, houseSenior, flats);
    }
}
